#include "core.h"

core::Point::Point(double x_cord, double y_cord) {
	x = x_cord;
	y = y_cord;
}

void core::Point::setRandomSpawnCords() {
	srand(std::time(nullptr));
	int random_variable = std::rand() % 100;
	x = random_variable;
	y = random_variable;
	std::cout << "Cords: x(" << x << "), y(" << y << ").";
}

core::Object::Object(Point P, int w, int h) {
	cords = P;
	width = w;
	height = h;
	object = new sf::RectangleShape();
	object->setSize(sf::Vector2f(w, h));

	texture.loadFromFile("img/dvd.png");
	object->setTexture(&texture);

	object->setPosition(cords.x, cords.y);
}

core::Object::~Object() {
	delete object;
}

sf::RectangleShape* core::Object::get() {
	return object;
}

void core::Object::updateCords() {
	if (cords.x >= (800 - width)) {
		x_speed *= -1;
	}
	else if (cords.x <= 0) {
		x_speed *= -1;
	}

	if (cords.y >= (600 - height)) {
		y_speed *= -1;
	}
	else if (cords.y <= 0) {
		y_speed *= -1;
	}
	cords.x += x_speed;
	cords.y += y_speed;
	object->setPosition(cords.x, cords.y);
}