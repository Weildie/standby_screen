#pragma once
#include <iostream>
#include <ctime>
#include <SFML/Graphics.hpp>

namespace core {
	struct Point {
		explicit Point(double x_cord = 0, double y_cord = 0);
		void setRandomSpawnCords();
		double x, y;
	};
	class Object {
	public:
		Object(Point P, int w, int h);
		~Object();
		sf::RectangleShape* get();
		void updateCords();
	private:
		double x_speed = 0.05;
		double y_speed = 0.05;
		Point cords;
		int width, height;
		sf::RectangleShape* object;
		sf::Texture texture;
	};
}
