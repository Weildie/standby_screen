# Standby Screen 

### step 1
```bash
git clone https://gitlab.com/Weildie/standby_screen.git
```

### step 2
Build project with CMake (actual verion for build - Visual Studio 19).

### Documentation
```cpp
using namespace core;

Point point = Point(); 		 // needs for Object init | (actually take 2 args)
point.setRandomSpawnCords(); // set random cords as default

// creation
Object* rect = new Object(point, 180, 100); // struct, width, high

// methods
rect->get() 		// gives a pointer at rect
rect->updateCords() // lets rect to move

```

### Additional
Program has 2 constants for window heigh and width.
Needs an image.
