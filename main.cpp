﻿#include "inc/core.h"

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
using namespace std;

int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "Standby Screen");
    
    core::Point point = core::Point();
    point.setRandomSpawnCords();

    core::Object* rect = new core::Object(point, 180, 100);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        rect->updateCords();

        window.clear();
        window.draw(*rect->get());
        window.display();
    }

    return 0;
}